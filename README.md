## LLVM Pass Example

This repository contains an example LLVM Pass using LLVM's new Pass Manager.

This repository has been tested on Ubuntu 20.04 and LLVM 10 installed from the default repositories.
```bash
# Install the dependencies
sudo apt install cmake llvm-10 llvm-10-dev llvm-10-tools

# Clone the repository
git clone git@gitlab.com:kevin-jude-concessao/example-llvm-pass.git

# Build the source code 
cd example-llvm-pass
mkdir build
cmake -DEP_LLVM_INSTALL_DIR=/usr/lib/llvm-10/ ..
make
```

The compiled plugin will be found at `build/lib/libExamplePrinter.so`. To test the LLVM Pass plugin,
```bash
# Assuming you are in the build directory
opt -load-pass-plugin lib/libExamplePrinter.so        \
    -passes=example-printer                           \
    -disable-output [path to your .bc or .ll file]
```

### For further reading:
* [2019 LLVM Developers’ Meeting: A. Warzynski “Writing an LLVM Pass: 101”](https://www.youtube.com/watch?v=ar7cJl2aBuU)
* [Writing LLVM Pass in 2018 — Part I: New Pass & Pass Manager in a Peek](https://medium.com/@mshockwave/writing-llvm-pass-in-2018-part-i-531c700e85eb)
* [llvm-tutor :fire:](https://github.com/banach-space/llvm-tutor)
