#include "llvm/IR/PassManager.h"
#include "llvm/Passes/PassBuilder.h"
#include "llvm/Passes/PassPlugin.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/ADT/ArrayRef.h"

using namespace llvm;

namespace {
struct ExamplePrinter : public PassInfoMixin<ExamplePrinter> {
  ExamplePrinter() = default;

  PreservedAnalyses run(Function &TheFunction, FunctionAnalysisManager &TheManager) {
    if (TheFunction.hasName()) {
      outs() << "Function: " << TheFunction.getName() << "\n";
      for (Function::iterator BBIter = TheFunction.begin(), BBEnd = TheFunction.end(); BBIter != BBEnd; ++BBIter) {
        outs() << "BB size: " << BBIter->size() << "\n"; 
      }
    }
    return PreservedAnalyses::all();
  }
};
} //namespace

extern "C" ::llvm::PassPluginLibraryInfo llvmGetPassPluginInfo() {
  return {LLVM_PLUGIN_API_VERSION, "ExamplePrinter", LLVM_VERSION_STRING,
          [](PassBuilder &ThePassBuilder) {
            ThePassBuilder.registerPipelineParsingCallback(
                [](StringRef PassName, FunctionPassManager &FPM,
                   ArrayRef<PassBuilder::PipelineElement>) {
                  if (PassName == "example-printer") {
                    FPM.addPass(ExamplePrinter());
                    return true;
                  }
                  return false;
                });
          }};
}
